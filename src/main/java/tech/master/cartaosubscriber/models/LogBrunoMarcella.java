package tech.master.cartaosubscriber.models;

import java.time.LocalTime;
import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import com.datastax.driver.core.LocalDate;

@Table
public class LogBrunoMarcella {
	@PrimaryKeyColumn(type=PrimaryKeyType.PARTITIONED)
	private UUID idLog;
	private String servico;
	@PrimaryKeyColumn(type=PrimaryKeyType.CLUSTERED)
	private LocalDate data;
	private LocalTime time;
	private String descricao;
	
	public UUID getIdLog() {
		return idLog;
	}
	public void setIdLog(UUID idLog) {
		this.idLog = idLog;
	}
	public String getServico() {
		return servico;
	}
	public void setServico(String servico) {
		this.servico = servico;
	}
	public LocalDate getData() {
		return data;
	}
	public void setData(LocalDate data) {
		this.data = data;
	}
	public LocalTime getTime() {
		return time;
	}
	public void setTime(LocalTime time) {
		this.time = time;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
